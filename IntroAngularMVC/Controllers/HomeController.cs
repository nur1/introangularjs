﻿using Fintech.Entity;
using IntroAngularMVC.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IntroAngularMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            using(var facade = new BankFacade())
            {
                var list = facade.GetAll();
                var bank = facade.GetByID(106);
                bank.BankName = "Bank Sadeli";
                bool result = facade.Update(bank);

                result = facade.Delete(106);
                //var b = new Banks();
                //b.AccountID = 10000;
                //b.BankName = "Bank NIaga";
                //b.HeadOffice = "Tambun, Bekasi";
                //b.ContactName = "Jaka";
                //b.ContactEmail = "jaka@gmail.com";
                //b.ContactPhone = "032546579";
                //bool result = facade.Insert(b);
                return View(list);
            }
            
        }
    }
}