﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IntroAngularMVC.DataAccess
{
    public class DB
    {
        private static string ConnStr = ConfigurationManager.ConnectionStrings["FINTECH"].ConnectionString;
        public static SqlConnection Connection
        {
            get
            {
                var conn = new SqlConnection(ConnStr);
                conn.Open();
                return conn;
            }
        }
    }
}