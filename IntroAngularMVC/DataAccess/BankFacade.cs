﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Fintech.Entity;
using Dapper;
using DapperExtensions;
using System.Data.SqlClient;

namespace IntroAngularMVC.DataAccess
{
    public class BankFacade : IDisposable
    {
        private string ConnStr = ConfigurationManager.ConnectionStrings["FINTECH"].ConnectionString;
        public void Dispose()
        {            
        }

        public IList<Banks> GetAll()
        {
            using(SqlConnection db = new SqlConnection(ConnStr))
            { 
                return db.GetList<Banks>().ToList();
            }
        }

        public Banks GetByID(int id)
        {
            using (var db = DB.Connection)
            { 
                return db.Get<Banks>(id);
            }
        }

        public bool Insert(Banks bank)
        {
            using(var db = DB.Connection)
            {                
                int id = db.Insert<Banks>(bank);
                return id > 0;
            }

        }

        public bool Update(Banks bank)
        {
            using (var db = DB.Connection)
            {
                return db.Update<Banks>(bank); 
            }

        }

        public bool Delete(int id)
        {
            using (var db = DB.Connection)
            {
                var pred = Predicates.Field<Banks>(b => b.BankID, Operator.Eq, id);
                return db.Delete<Banks>(pred);
            }

        }
    }
}